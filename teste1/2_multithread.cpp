#include<iostream>
#include<sstream>
#include<math.h>
#include<vector>
#include<thread>
#include<mutex>
#include <ctime>

using namespace std;


const long double ALLOCATION_CONST = pow(2, 20);
const int MAX_RANDOM = 1000;


long double allocationSize;
uint64_t* values;
long double averageValue = 0;

mutex averageMutex;




uint64_t generateRandom(){
    return ( rand() % (MAX_RANDOM + 1) );
}




void generateRandomAndCalculateAverageParcial(long double taskSize, long double beginning, long double ending){
    long unsigned int i;
    for(i = beginning; i < ending; i++){
        values[i] = generateRandom();
    }

    long double myAverage = 0;
    for(i = beginning; i < ending; i++){
        myAverage += values[i];
    }
    myAverage = myAverage / taskSize;

    averageMutex.lock();
    averageValue += myAverage;
    averageMutex.unlock();
}




int main(int argc, char* argv[]){

    clock_t begin = clock();

    unsigned int N = 64;
    unsigned int k = 2;

    if(argc >= 3){
        istringstream ss(argv[1]);
        if (!(ss >> N)){
            exit(-1);
        }

        istringstream ss2(argv[2]);
        if(!(ss2 >> k)){
            exit(-1);
        }
    }

    allocationSize = N * ALLOCATION_CONST;

    values = (uint64_t*) malloc (allocationSize * sizeof(uint64_t) );

    vector<thread> threads;

    if(values != NULL){
        cout << "=====MULTITHREAD TASK=====\n";

        long unsigned int i;
        for(i = 0; i < k; i++){
            long double taskSize = allocationSize/k;
            long double beginning = taskSize * i;
            long double ending = beginning + taskSize - 1;
            threads.push_back(thread(generateRandomAndCalculateAverageParcial, taskSize, beginning, ending));
        }

        for(i = 0; i < k; i++){
            threads[i].join();
        }

	averageValue = averageValue / k;

        cout << "Average: " << averageValue << "\n";

        free(values);

	clock_t end = clock();
        double elapsed_secs = double((end - begin)/k) / CLOCKS_PER_SEC;
        cout << elapsed_secs << " seconds elapsed\n";
    }

    return 0;
}
