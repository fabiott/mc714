# Teste 1 - MC 714 - 2s2017 #

Fábio Takahashi Tanniguchi - RA 145980

### Introdução ###

Este teste consiste na implementação de três programas que armazenem números aleatórios e depois calculem a média deles. Os três se diferenciam pela abordagem utilizada:

* abordagem 1: programa sequencial, fazendo os passos de forma sequencial;
* abordagem 2: programa multithread, utilizando threads para cada uma gerar os números e calcular a média parcialmente e concomitantemente;
* abordagem 3: programa multiprocessos, utilizando processos filhos para cada uma gerar os números e calcular a média parcialmente e concomitantemente.

### Resultados obtidos ###

Execuções realizadas em notebook com a seguinte configuração:

* processador Intel Core i5-3317U;
* memória RAM de 8GB;
* sistema operacional Ubuntu 17.04 64 bit.

| Número de threads/processos | Sequencial | Multithread | Multiprocessos |
|-----------------------------|------------|-------------|----------------|
|            1                |  1,4421 s  |             |                |
|            2                |            |  17,4039 s  |   0,0007 s     |
|            4                |            |   9,8374 s  |   0,0010 s     |
|            8                |            |   5,5437 s  |   0,0019 s     |

![Resultados obtidos](/teste1/chart.png)

### Conclusões ###

Como é possível perceber, a abordagem multiprocessos mostrou melhor desempenho. Já a abordagem multithread, nos testes realizados, mostrou-se a pior nos cenários utilizado.

Inicialmente, esperava que tanto a abordagem multiprocessos como a multithread se mostrassem mais rápidas que a sequencial.

O pior desempenho da abordagem multithread pode ter se dado (avaliando de forma especulativa) por custos de criação e execução das estruturas de threads em C++ e pelo Mutex utilizado para substituir o valor de averageValue. A implementação da função executada pelas threads também deve ser um fator, apesar de não ter encontrado até o momento oportunidades de melhorias no código.

Outra observação a se fazer é em relação ao tempo crescente proporcionalmente ao número de processos na abordagem multiprocessos. Novamente avaliando de forma especulativa, isso pode ter se dado em virtude de uma não utilização de cores físicos distintos no processador, fazendo com que houvesse maior concorrência entre os processos nos testes com 4 e 8 processos.

De qualquer forma, a abordagem multiprocessos foi consideravelmente mais rápida.

