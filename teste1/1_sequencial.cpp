#include<iostream>
#include<sstream>
#include<math.h>
#include <ctime>

using namespace std;

const long double ALLOCATION_CONST = pow(2, 20);
const int MAX_RANDOM = 1000;

uint64_t generateRandom(){
    return ( rand() % (MAX_RANDOM + 1) );
}

int main(int argc, char* argv[]){

    clock_t begin = clock();

    unsigned int N = 64;

    if(argc >= 2){
        istringstream ss(argv[1]);
        if (!(ss >> N)){
            exit(-1);
        }
    }

    long double allocationSize = N * ALLOCATION_CONST;

    uint64_t* values = (uint64_t*) malloc (allocationSize * sizeof(uint64_t) );

    if(values != NULL){
        cout << "=====SEQUENTIAL TASK=====\n";
        long unsigned int i;
        for(i = 0; i < allocationSize; i++){
            values[i] = generateRandom();
        }

        long double averageValue = 0;

        for(i = 0; i < allocationSize; i++){
            averageValue += values[i];
        }
        averageValue = averageValue / allocationSize;

        cout << "Average: " << averageValue << "\n";

        free(values);

	clock_t end = clock();
        double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
        cout << elapsed_secs << " seconds elapsed\n";
    }

    return 0;
}
