#include <iostream>
#include <sstream>
#include <math.h>
#include <vector>
#include <mutex>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <ctime>

using namespace std;

const long double ALLOCATION_CONST = pow(2, 20);
const int MAX_RANDOM = 1000;
const char* SHDMEM_VALUES = "MC714_VALUES";
const char* SHDMEM_AVG = "MC714_AVG";


long double allocationSize;
uint64_t* values;
double *averageValue;




uint64_t generateRandom(){
    return ( rand() % (MAX_RANDOM + 1) );
}




int main(int argc, char* argv[]){

    clock_t begin = clock();

    unsigned int N = 64;
    unsigned int k = 2;

    if(argc >= 3){
        istringstream ss(argv[1]);
        if (!(ss >> N)){
            exit(-1);
        }

        istringstream ss2(argv[2]);
        if(!(ss2 >> k)){
            exit(-1);
        }
    }

    allocationSize = N * ALLOCATION_CONST;

    cout << "=====MULTIPROCESS TASK=====\n";

    long unsigned int i;

    // open the memory
    int sharedMemoryValues = shm_open(SHDMEM_VALUES, O_CREAT | O_EXCL | O_RDWR, S_IRWXU | S_IRWXG);
    // attach the shared memory segment
    ftruncate(sharedMemoryValues, allocationSize*sizeof(uint64_t*));
    // allocating the shared memory
    values = (uint64_t*) mmap(NULL, allocationSize*sizeof(uint64_t), PROT_READ | PROT_WRITE, MAP_SHARED, sharedMemoryValues, 0);

    int sharedMemoryAvg = shm_open(SHDMEM_AVG, O_CREAT | O_EXCL | O_RDWR, S_IRWXU | S_IRWXG);
    ftruncate(sharedMemoryAvg, sizeof(double));
    averageValue = (double*) mmap(NULL, sizeof(double), PROT_READ | PROT_WRITE, MAP_SHARED, sharedMemoryAvg, 0);
    *averageValue = 0.0;

    pid_t myPid = getpid();
    vector<pid_t> pids;

    for(i = 0; i < k; i++){

        long double taskSize = allocationSize/k;
        long double beginning = taskSize * i;
        long double ending = beginning + taskSize - 1;

        pid_t pid = fork();
        pids.push_back(pid);

	if(pid == 0){ // child
		for(i = beginning; i < ending; i++){
		    values[i] = generateRandom();
		}

		long double myAverage = 0;
		for(i = beginning; i < ending; i++){
		    myAverage += values[i];
		}
		myAverage = myAverage / taskSize;

		*averageValue += myAverage;

		break;
        }
    }

    if(getpid() == myPid){

	int status;
	for(i = 0; i < pids.size(); i++)
		waitpid(pids[i], &status, 0);

	*averageValue = *averageValue / k;

        cout << "Average: " << *averageValue << "\n";

        close(sharedMemoryAvg);
        close(sharedMemoryValues);

        shm_unlink(SHDMEM_AVG);
        shm_unlink(SHDMEM_VALUES);

        clock_t end = clock();
        double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
        cout << elapsed_secs << " seconds elapsed\n";
    }

    return 0;
}
