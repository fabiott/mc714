# Teste 2 - MC 714 - 2s2017 #

Fábio Takahashi Tanniguchi - RA 145980

### Introdução ###

Este teste consiste na implementação de um sistema básico de disseminação de informação realizando testes com diferentes números de processos e com diferentes probabilidades de parada após fofocar sem êxito:

### Resultados obtidos ###

Execuções single (único host) realizadas em desktop com a seguinte configuração:

* processador Intel Core i7-3770K;
* memória RAM de 32GB;
* sistema operacional Windows 10 64 bit, versão 1703, build 15063.540;
* máquina virtual Java JDK 1.8.0, revision 66, 64 bit.

|   N   |   K   | Min Gossiping | Max Gossiping | Medium Gossiping | Processes with info | Time elapsed (s) | Total attempts | Percentage of success |
|-------|-------|---------------|---------------|------------------|---------------------|------------------|----------------|-----------------------|
|    10 |   2   |       0       |         5     |           1      |              8      |            0     |       16       |          50.00        |
|    10 |   5   |       1       |        20     |           5      |             10      |            0     |       57       |          17.54        |
|    10 |   8   |       1       |       552     |          84      |             10      |            0     |      848       |           1.18        |
|   100 |   2   |       0       |       659     |          91      |             95      |            2     |     9112       |           1.04        |
|   100 |   8   |       0       |       432     |          97      |             99      |            2     |     9747       |           1.02        |
|  1000 |   2   |       0       |       465     |          15      |            956      |            3     |    15137       |           6.32        |
|  1000 |   8   |       0       |       392     |          46      |            996      |            6     |    46028       |           2.16        |
| 10000 |   2   |       0       |       295     |           1      |            876      |           37     |    13002       |           6.74        |
| 10000 |   8   |       0       |       322     |          20      |           9992      |           41     |   208320       |           4.80        |
| 50000 |   2   |       0       |       260     |           1      |           8852      |          157     |    85659       |          10.33        |
| 50000 |   5   |       0       |       318     |           7      |          48138      |          282     |   365431       |          13.17        |
| 50000 |   8   |       0       |       270     |          19      |          49867      |          285     |   953931       |           5.23        |

### Conclusões ###

Como se pode perceber pela tabela, houve considerável melhora no número de processos com informação ao executar com K=8 (probabilidade de parada de 1/8), para mesmo N (número de clientes participando do processo de fofoca).

A implementação feita em Java utilizando RabbitMQ conseguiu comportar, na máquina descrita, execuções com N=50000 em tempo razoável dentro do que se estava disposto a aguardar.

Tirando as execuções com N=10, que podem ser consideradas como casos básicos deste teste, todas as demais tiveram no mínimo um processo sem a informação, evidenciando que esse fato é comum na disseminação por escolha randômica com parada de probabilidade 1/k ao detectar envio não-útil.
