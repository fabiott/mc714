import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeoutException;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

public class Teste2MainServer {

	public static final String DEFAULT_CLIENTS_HOST = "localhost";

	public static final String QUEUE_NAME_PATTERN = "queue_";
	public static final String EXCHANGE_NAME = "teste2exchange";
	public static final String RESULTS_QUEUE = "results";

	// default messages used by this program
	public static final String FOFOCA_MSG = "fofoca";
	public static final String OK_MSG = "legal";
	public static final String NOTNEW_MSG = "jasabia";

	public static final String SERVER_NAME = "server";

	public static void main(String args[]) throws IOException,
			TimeoutException, InterruptedException {
		int n = Integer.valueOf(args[0]);
		int k = Integer.valueOf(args[1]);

		System.out.println("N = " + n + " | K = " + k);

		ConnectionFactory factory = new ConnectionFactory();
		if (args.length > 2) { // clients will be executed separately
			factory.setHost(args[2]);
		} else {
			factory.setHost(DEFAULT_CLIENTS_HOST);
		}
		factory.setConnectionTimeout(100);

		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		System.out.println("Connection successful with " + factory.getHost());

		channel.exchangeDeclare(EXCHANGE_NAME, "direct", false);
		channel.basicQos(1);
		
		Teste2ResultsHost consolidationConsumer = null;
		if (args.length <= 2) {
			// starting results queue
			String queueName = RESULTS_QUEUE;
			channel.queueDeclare(queueName, false, true, false, null);
			channel.queueBind(queueName, EXCHANGE_NAME, queueName);
			consolidationConsumer = new Teste2ResultsHost(channel,
					RESULTS_QUEUE, new Date().getTime(), n);
			consolidationConsumer.start();			
			channel.basicConsume(queueName, consolidationConsumer);
			
			System.out.println("Created results queue");
		}
		
		ArrayList<Teste2Client> clients = new ArrayList<Teste2Client>();
		for (int i = 0; i < n; i++) {
			String queueName = QUEUE_NAME_PATTERN + i;
			channel.queueDeclare(queueName, false, false, false, null);
			channel.queueBind(queueName, EXCHANGE_NAME, queueName);

			if (args.length <= 2) {
				Teste2Client client = new Teste2Client(channel,
						QUEUE_NAME_PATTERN + i, n, k);
				clients.add(client);
				client.start();				
				channel.basicConsume(queueName, client);
			}
		}
		System.out.println("Created gossiping queues");
		System.out.println("Gossiping started! Wait...");

		Random random = new Random();
		String nextConsumer = QUEUE_NAME_PATTERN + random.nextInt(n);
		String messageToConsumer = nextConsumer + ","
				+ Teste2MainServer.FOFOCA_MSG;
		channel.basicPublish(EXCHANGE_NAME, nextConsumer,
				MessageProperties.TEXT_PLAIN, messageToConsumer.getBytes());
		
		System.out.println(" - Server sent first message to " + nextConsumer);

		for (Teste2Client client : clients) {
			client.getThread().join();
		}

		if (args.length <= 2) { // server started the clients
			consolidationConsumer.getThread().join();

			channel.close();
			connection.close();
		}
	}
}
