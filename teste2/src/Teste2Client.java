import java.io.IOException;
import java.util.Date;
import java.util.Random;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.MessageProperties;

public class Teste2Client extends DefaultConsumer implements Runnable {

	public static final String DEFAULT_SERVER_HOST = "localhost";

	private static final int WAITING_INTERVAL_MS = 1000;
	private static final int DEFAULT_WAITING_S = 15;

	private Thread thread;

	private Random random = new Random();

	private String name;
	private Integer countdown = DEFAULT_WAITING_S;
	private Integer n;
	private Integer k;

	private Integer timesGossiped = 0;
	private Integer timesGossipedWithSuccess = 0;
	private String gossipValue;

	public Teste2Client(Channel channel, String name, Integer N, Integer K) {
		super(channel);
		this.name = name;
		this.n = N;
		this.k = K;
	}

	public void run() {
		while (countdown > 0 && !thread.isInterrupted()) {
			try {
				Thread.sleep(WAITING_INTERVAL_MS);
			} catch (InterruptedException e) {
				// do nothing
			}
			this.countdown--;
		}

		try { // cancel consuming relationship
			this.getChannel().basicCancel(this.getConsumerTag());
		} catch (IOException e) {
			return;
		}
	}

	@Override
	public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
			throws IOException {
		String message = new String(body, "UTF-8");
		if (!thread.isInterrupted()) {
			countdown = 5;
		}
		
		//System.out.println(" - " + this.name + " recebeu mensagem " + message);

		if (message.equals(Teste2MainServer.NOTNEW_MSG)) { // gossip not new for the other client

			if (this.random.nextInt(this.k) == 0) { // probability 1/k
				// stop gossiping
				countdown = 0;
				thread.interrupt();
			} else {
				// continue gossiping
				String sendingQueueName = Teste2MainServer.QUEUE_NAME_PATTERN + this.random.nextInt(this.n);
				String sendingMessage = sendingQueueName + "," + Teste2MainServer.FOFOCA_MSG;
				this.getChannel().basicPublish(Teste2MainServer.EXCHANGE_NAME, sendingQueueName,
						MessageProperties.TEXT_PLAIN, sendingMessage.getBytes());

				this.timesGossiped++;
			}

		} else if (message.equals(Teste2MainServer.OK_MSG)) {
			String sendingQueueName = Teste2MainServer.QUEUE_NAME_PATTERN + this.random.nextInt(this.n);
			String sendingMessage = sendingQueueName + "," + Teste2MainServer.FOFOCA_MSG;
			this.getChannel().basicPublish(Teste2MainServer.EXCHANGE_NAME, sendingQueueName,
					MessageProperties.TEXT_PLAIN, sendingMessage.getBytes());

			this.timesGossiped++;
			this.timesGossipedWithSuccess++;

		} else {
			String[] list = message.split(",");

			String sender = list[0];
			String messageToSender;

			// send response to the sender
			if (this.gossipValue == null || this.gossipValue.isEmpty()) {
				// new gossip
				this.gossipValue = list[1];
				messageToSender = Teste2MainServer.OK_MSG;
			} else {
				// not new
				messageToSender = Teste2MainServer.NOTNEW_MSG;
			}
			this.getChannel().basicPublish(Teste2MainServer.EXCHANGE_NAME, sender, MessageProperties.TEXT_PLAIN,
					messageToSender.getBytes());

			// keep gossiping - like Mamma Bruschetta, Leao Lobo and Nelson Rubens
			String sendingQueue = Teste2MainServer.QUEUE_NAME_PATTERN + this.random.nextInt(this.n);
			String sendingMessage = sendingQueue + "," + Teste2MainServer.FOFOCA_MSG;
			this.getChannel().basicPublish(Teste2MainServer.EXCHANGE_NAME, sendingQueue, MessageProperties.TEXT_PLAIN,
					sendingMessage.getBytes());
			this.timesGossiped++;
		}

		this.getChannel().basicAck(envelope.getDeliveryTag(), false);
	}

	@Override
	public void handleCancelOk(String consumerTag) {

		// send consolidation report with info about this gossiping process
		String messageToConsumer = this.name.concat(",").concat(String.valueOf(this.timesGossiped)).concat(",")
				.concat(String.valueOf(new Date().getTime())).concat(",")
				.concat(String.valueOf(this.timesGossipedWithSuccess));

		try {
			this.getChannel().basicPublish(Teste2MainServer.EXCHANGE_NAME, Teste2MainServer.RESULTS_QUEUE,
					MessageProperties.PERSISTENT_TEXT_PLAIN, messageToConsumer.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}

		super.handleCancelOk(consumerTag);
	}

	public void start() {
		if (thread == null) {
			thread = new Thread(this, name);
			thread.start();
		}
	}

	public Thread getThread() { // method useful for main server and/or main client to join all the threads
		return this.thread;
	}

}
