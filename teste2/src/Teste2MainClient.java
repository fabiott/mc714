import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

// use this main if clients will be executed in different host 
// so it is necessary to execute clients in batch separately 

public class Teste2MainClient {

	public static void main(String args[]) throws IOException,
			TimeoutException, InterruptedException {
		int n = Integer.valueOf(args[0]);
		int k = Integer.valueOf(args[1]);

		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		factory.setConnectionTimeout(100);

		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		channel.exchangeDeclare(Teste2MainServer.EXCHANGE_NAME, "direct", false);
		channel.basicQos(1);

		ArrayList<Teste2Client> clients = new ArrayList<Teste2Client>();
		for (int i = 0; i < n; i++) {
			String queueName = Teste2MainServer.QUEUE_NAME_PATTERN + i;
			channel.queueDeclare(queueName, false, false, false, null);
			channel.queueBind(queueName, Teste2MainServer.EXCHANGE_NAME, queueName);

			if (args.length <= 2) {
				Teste2Client defaultConsumer = new Teste2Client(channel,
						Teste2MainServer.QUEUE_NAME_PATTERN + i, n, k);
				clients.add(defaultConsumer);
				defaultConsumer.start();
				channel.basicConsume(queueName, defaultConsumer);
			}
		}

		System.out.println("Clientes criados");

		String queueName = Teste2MainServer.RESULTS_QUEUE;
		channel.queueDeclare(queueName, false, false, false, null);
		channel.queueBind(queueName, Teste2MainServer.EXCHANGE_NAME, queueName);
		Teste2ResultsHost consolidationConsumer = new Teste2ResultsHost(
				channel, Teste2MainServer.RESULTS_QUEUE, new Date().getTime(), n);
		consolidationConsumer.start();
		channel.basicConsume(queueName, consolidationConsumer);

		for (Teste2Client client : clients) {
			client.getThread().join();
		}
		consolidationConsumer.getThread().join();

		channel.close();
		connection.close();
	}

}
