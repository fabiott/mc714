import java.io.IOException;
import java.util.concurrent.TimeUnit;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class Teste2ResultsHost extends DefaultConsumer implements Runnable {

	private static final int DEFAULT_WAITING_SECONDS = 20;

	private Thread thread;

	private String name;

	private Integer countdown = DEFAULT_WAITING_SECONDS;

	private Integer minGossiped = null;
	private Integer maxGossiped = 0;

	private Long begin;
	private Long end = null;

	private Integer qtyWithInfo = 0;
	private Integer qtySent = 0;
	private Integer qtyUsefulSent = 0;
	
	private int n;

	public Teste2ResultsHost(Channel channel, String name, Long begin, int n) {
		super(channel);
		this.name = name;
		this.begin = begin;
		this.n = n;
	}

	public void run() {
		while (countdown > 0) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// do nothing
			}
			this.countdown--;
		}

		try { // cancel consuming relationship
			this.getChannel().basicCancel(this.getConsumerTag());
		} catch (IOException e) {
			// do nothing
		}
	}

	@Override
	public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
			throws IOException {
		String message = new String(body, "UTF-8");
		this.countdown = DEFAULT_WAITING_SECONDS; // wait again for a new result

		String[] list = message.split(",");

		int timesGossiped = Integer.valueOf(list[1]);
		long lastime = Long.valueOf(list[2]);

		this.qtySent += timesGossiped;
		this.qtyUsefulSent += Integer.valueOf(list[3]);

		if (this.minGossiped == null) {
			this.minGossiped = timesGossiped;
		} else {
			if (this.minGossiped > timesGossiped) {
				this.minGossiped = timesGossiped;
			}
		}

		if (this.maxGossiped == null) {
			this.maxGossiped = timesGossiped;
		} else {
			if (this.maxGossiped < timesGossiped) {
				this.maxGossiped = timesGossiped;
			}
		}

		if (timesGossiped > 0) {
			this.qtyWithInfo++;
		}

		if (this.end == null) {
			this.end = lastime;
		} else {
			if (this.end < lastime) {
				this.end = lastime;
			}
		}

		this.getChannel().basicAck(envelope.getDeliveryTag(), false); // send a rabbitmq-default ack

	}

	@Override
	public void handleCancelOk(String consumerTag) {
		long timeElapsed = TimeUnit.MILLISECONDS.toSeconds(this.end - this.begin);

		// print a gossiping report
		System.out.println("----------------------------------");
		System.out.println("Minimum gossiped: " + this.minGossiped);
		System.out.println("Maximum gossiped: " + this.maxGossiped);
		System.out.println("Medium gossiped: " + (this.qtySent) / n);
		System.out.println("----------------------------------");
		System.out.println("With information: " + this.qtyWithInfo);
		System.out.println("----------------------------------");
		System.out.println("Gossip attempts: " + this.qtySent);
		System.out.println("Useful gossip attempts: " + this.qtyWithInfo);
		System.out.println("----------------------------------");
		System.out.println("Gossiping time elapsed (seconds): " + timeElapsed);
		System.out.println("----------------------------------");

		super.handleCancelOk(consumerTag);
	}

	public void start() {
		if (thread == null) {
			thread = new Thread(this, name);
			thread.start();
		}
	}

	public Thread getThread() { // method useful for main server and/or main client to join all the threads
		return thread;
	}
}
